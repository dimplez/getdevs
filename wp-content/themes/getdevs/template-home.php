<?php 

/* Template Name: Home */ 

get_header(); ?>

    <main role="main">
        <div class="index">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="entry">
                        <div class="center">
                            <h3 class="home-title"><?php the_title(); ?></h3>
                        </div>

                        <?php remove_filter ('the_content', 'wpautop'); ?>
                        <?php the_content(); ?>
                        
                    </div>
                    <a href="#" class="scroll-btn"><span class="glyphicon glyphicon-chevron-down"></span></a>
                </article>

            <?php endwhile; else: ?>

                <article>
                    <h2>Sorry, nothing to display.</h2>
                </article>

            <?php endif; ?>

        </div>
    </main>

<!-- 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/parallax.translate.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/jquery.scale.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/jquery.perspective.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/jquery.skew.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/jquery.swipe.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/parallax.rotate.js"></script>
-->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/jquery.parallax.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/parallax.opacity.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/jquery.transform.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/parallax.position.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/parallax.background.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parallax/custom.scripts.js"></script>

<?php get_footer(); ?>