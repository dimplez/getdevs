<?php get_header(); ?>
	<!-- Modal Div -->
	<div id="request-modal" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
		    <div class="modal-content">
		    	<form role="form" method="POST" action="">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			        	<h4 class="modal-title" id="myModalLabel">Request</h4>
			      	</div>
			      	<div class="modal-body">
		        		<div class="form-group">
						    <div class="input-group">
							    <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
							    <input name="request-name" class="form-control" type="text" placeholder="Name*" required>
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="input-group">
							    <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
							    <input name="request-email" class="form-control" type="email" placeholder="Email*" required>
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="input-group">
							    <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
							    <input name="request-company" class="form-control" placeholder="Company" type="text">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="input-group">
							    <div class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></div>
							    <input name="request-phone" class="form-control input-mask-phone" placeholder="Phone Number" type="text" min="0">
						    </div>
					  	</div>

					  	<!-- Captcha -->
				      	<div class="row">
						  	<div class="col-xs-2">
						    	<input id="num1" name="num1" type="text" class="form-control text-center" value="<?php echo rand( 0, 5 ) ?>" readonly>
						  	</div>
						  	<div class="col-xs-1 text-center"> + </div>
						  	<div class="col-xs-2">
						    	<input id="num2" name="num2" type="text" class="form-control text-center" value="<?php echo rand( 6, 9 ) ?>" readonly>
						  	</div>
						  	<div class="col-xs-1 text-center"> = </div>
						  	<div class="col-xs-3">
						    	<input id="captcha" name="captcha" maxlength="2" type="number" min="0" max="20" class="form-control text-center" placeholder="?" required>
						  	</div>
						  	<div class="col-xs-12">
						  		<br />
						  		<div class="captcha-alert alert alert-danger hide" role="alert">
  									<strong><span class="alert-link">Wrong Captcha!</span></strong>
								</div>
							</div>
						</div>
			      	</div>
			      	<div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <input type="submit" class="btn btn-primary" name="request-btn" value="Request">
			      	</div>
		      	</form>
		    </div>
	  	</div>
	</div>
	<main role="main">
		<section class="profile">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="center"> 
					<div class="entry">

						<a class="btn btn-warning" href="/software-development-team" role="button">
							<span class="glyphicon glyphicon-arrow-left"></span>
							Full list of available talent
						</a>

						<?php if (have_posts()): while (have_posts()) : the_post(); 

						// Candidate Info
						$info = resume_portal_get_info_id( get_the_ID() ); 

						//candidate Skills
						$skills =  resume_portal_get_skills_decoded( get_the_ID(), true );

						// File Resume Link
						$resume_link = get_post_meta( get_the_ID(), 'resume_upload', true); 
						
						// Mail Functions
						if($resume_link) {
							resume_portal_request_file($resume_link);
						}
						
						// Sort by Descending based on score
						// usort($skills, 'resume_portal_skill_sort_score_desc');

						if ( has_post_thumbnail() ) {
							the_post_thumbnail(	
								array(225, 225), 
								array(
									'class' => 'attachment-profile-image center-block',
									'alt' =>  get_the_title(),
									'title' => get_the_title()
								)
							);
						} else {
							echo '<img src="'. get_template_directory_uri() .'/img/default-profile.png" class="attachment-profile-image center-block" height="255" width="255" />';
						}
						?>

						<div class="text-center">
							<h1><?php the_title(); ?></h1>
							<small><strong><?php echo $info['yrs_exp']; ?></strong> Years of Industry Experience</small>
							<p>
								<small>Experienced <strong><?php echo ucwords( str_replace('-', ' ',$skills[0]['skill']) ); ?></strong> Candidate</small>
							</p>
						</div>

						<table class="table table-hover table-condensed table-striped table-bordered">
							<thead>
						        <tr>
						        	<th class="text-center"><small> Technology </small></th>
						          	<th class="text-center"><small> Self Rating </small></th>
						          	<th class="text-center"><small> Years of Experience </small></th>
					        	</tr>
					     	</thead>
					     	<tbody>
					     		<?php foreach ($skills as $skill => $value) : ?>
					     		<tr class="text-center">
					     			<td>
					     			<?php 
					     				// for random class color
										$colors = array(
											1 => 'default', 
											2 => 'primary', 
											3 => 'success', 
											4 => 'info', 
											5 => 'warning', 
											6 => 'danger'
										);
										$new_color = array_rand($colors, 1);
										echo '<span class="label label-'. $colors[$new_color] .'">'. $value['skill'] .'</span> ';
					     			 ?>
					     			 </td>
					     			<td><?php echo $value['score']; ?></td>
					     			<td><?php echo $value['yrs_exp']; ?></td>
					     		</tr>
					     		<?php endforeach; ?>
					     	</tbody>
						</table>
					
						<hr />
						<div>
							<blockquote>
							  <h2>Information</h2>
							</blockquote>
							<dl class="dl-horizontal">
								<dt>Candidate Number : </dt>
								<dd><?php echo $info['c_number']; ?></dd>
								<dt>Position/Title : </dt>
								<dd><?php echo $info['c_position']; ?></dd>
							</dl>

							<blockquote>
							  <h2>Educational Background</h2>
							</blockquote>
							<dl class="dl-horizontal">
								<dt>Education : </dt>
								<dd><?php echo $info['education']; ?></dd>
								<dt>Degree : </dt>
								<dd><?php echo $info['degree']; ?></dd>
								<dt>Year Graduated : </dt>
								<dd><?php echo $info['yrs_grad']; ?></dd>
							</dl>

							<blockquote>
							  <h2>Code Sample</h2>
							</blockquote>

						  	<pre><code><?php echo str_replace("\t", "    ", stripslashes($info['code_sample']) ); ?></code></pre>
						  	
						  	<hr />
						</div>
						<?php if( ! empty( $resume_link ) ) : ?>

						<button type="button" class="btn btn-danger btn-sm" id="request-resume"
						data-toggle="modal" data-target="#request-modal" >
	     					<span class="glyphicon glyphicon-file"></span> Request Resume
	     				</button>

	     				<?php endif; ?>
					<?php endwhile; else: ?>
						<h2><?php _e( 'Sorry, nothing to display.' ); ?></h2>
					<?php endif; ?>

					</div>
				</div>

			</article>
		</section>
	</main>

	<script src="<?php echo plugins_url('resume-portal'); ?>/assets/js/jquery.maskedinput.min.js"></script>
	<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$('.input-mask-phone').mask('(9999)-999-9999');
		});
	})(jQuery);
	</script>
<?php get_footer(); ?>