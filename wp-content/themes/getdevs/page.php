<?php get_header(); ?>

	<main role="main">
		<section class="block">

			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="center">
						<div class="entry center"> 

							<div class="text-center">
								<?php if ( has_post_thumbnail()) { // Check if Thumbnail exists 
										the_post_thumbnail();
									} 
								?>
							</div>

							<?php the_content(); ?>
                        </div>
					</div>
				</article>

			<?php endwhile; else: ?>

				<article>
					<div class="entry"> 
                        <div class="center">
							<h2><?php _e( 'Sorry, nothing to display.' ); ?></h2>
						</div>
					</div>
				</article>

			<?php endif; ?>

		</section>
	</main>
<?php get_footer(); ?>
