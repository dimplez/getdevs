<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="block">
			<div class="center bg-white clearfix"> 
                <div class="entry with-sidebar">
					<h1><?php _e( 'Tag Archive: ', 'html5blank' ); echo single_tag_title('', false); ?></h1>

					<?php get_template_part('loop'); ?>

					<?php get_template_part('pagination'); ?>
				</div>

				<?php get_sidebar(); ?>
			</div>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
