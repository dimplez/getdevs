<?php get_header(); ?>

	<!-- URL: website.com/software-development-team/-->
	<main role="main">
		<section class="block">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="center"> 
					<div class="entry with-sidebar">

					<?php // Get the content of the People Page
					query_posts('post_type=page&page_id=12');
					while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>

					<?php 
					// Get all the Skills Parent Category
					$args = array(
						'orderby'	=> 'custom_sort',
						'order'		=> 'ASC',
						'parent' 	=> 0,
						'taxonomy'  => 'skills',
						'hide_empty'=> 1
					); 
					$categories = get_categories( $args );
			
					foreach ($categories as $category) : 

						echo '<div><h2 class="text-underline">'. $category->name .'</h2></div>';

						// Get the Users under this Parent Category by slug
						$candidates = get_posts( 
							array (
								'showposts' => 3,
							    'post_type' => 'resume_portal',
							    'tax_query' => array (
							        array(
								        'taxonomy' => 'skills',
								        'field' => 'slug',
								        'terms' => $category->slug
								    )
							    )
							)
						); ?>
						<table class="table table-hover table-condensed table-striped table-bordered">
							<thead>
						        <tr>
						          	<th class="text-center"> </th>
						        	<th class="text-center"><small> Top Skills </small></th>
						          	<th class="text-center"><small> Years of Experience </small></th>
						          	<th class="text-center"><small> # </small></th>
					        	</tr>
					     	</thead>
					     	<tbody>

							<?php // candidates info per row
							foreach ($candidates as $candidate) :

							$info = resume_portal_get_info_id( $candidate->ID );
							$skills = resume_portal_get_skills_decoded( $candidate->ID, false ); 

							if($info['published'] == 'Y') : ?>

								<tr class="text-center" data-yrs="<?php echo $info['yrs_exp']; ?>">
									<td>
										<a alt="View Profile" title="View Profile" 
										href="<?php echo $candidate->guid; ?>" class="btn btn-primary btn-sm view-profile">
					     					<span class="glyphicon glyphicon-user"></span>
					     				</a>
					     			</td>
									<td>
									<?php 
									// show only 3 skills
									$skills = array_slice($skills, 0, 3);

									foreach ($skills as $skill) :
									// for random class color
									$colors = array(
										1 => 'default', 
										2 => 'primary', 
										3 => 'success', 
										4 => 'info', 
										5 => 'warning', 
										6 => 'danger'
									);
								
									$link = get_term_by( 'name', $skill->skill, 'skills', 'ARRAY_A' );

									$new_color = array_rand($colors, 1); ?>
										<a class="label label-<?php echo $colors[$new_color]; ?>" href="/skills/<?php echo  $link['slug']; ?>"><?php echo $skill->skill; ?></a>
									<?php endforeach; // $skill ?>

									</td>
									<td><small><?php echo $info['yrs_exp']; ?></small></td>
									<td><small><?php echo $info['c_number']; ?></small></td>
								</tr>

							<?php endif; endforeach; //  $candidate ?>
							</tbody>
						</table>
						<hr />
					<?php endforeach; // $category ?>
					</div>

					<aside class="sidebar">
						<?php get_search_form(); ?>
						
						<h2>Skills</h2>
						<ul class="skill-list">
						<?php 
						$args = array(
							'orderby'	=> 'name',
							'parent' 	=> 0,
							'taxonomy'  => 'skills',
							'hide_empty'=> 0
						); 
						$sidebar_categories = get_categories( $args );
						foreach ($sidebar_categories as $category) : ?>
							<li><a href="/skills/<?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
								<ul class="collapse">
									<?php 
										$args = array(
												'parent'                   => $category->term_id,
												'orderby'                  => 'name',
												'order'                    => 'ASC',
												'hide_empty'               => 0,
												'taxonomy'                 => 'skills'
											); 
										$child_categories = get_categories( $args );

										foreach ($child_categories as $child_category) : ?>
											<li><a href="/skills/<?php echo $child_category->slug; ?>"><?php echo $child_category->name; ?></a></li>
									<?php endforeach;?>
								</ul>
							</li>
						<?php endforeach; ?>
						</ul>
					</aside>
				</div>
			</article>
		</section>
	</main>

<?php get_footer(); ?>