(function($) {
	$(window).ready(function() {
		
		// For Simple Captcha
		var total = parseInt( $('#num1').val() ) + parseInt( $('#num2').val() );

		$('input[name="submit"]').click(function(e) {
			if(total != $('#captcha').val() ) {
				e.preventDefault();
				$('.captcha-alert').removeClass('hide').addClass('show');
			} else {
				$('.captcha-alert').removeClass('show').addClass('hide');
			}
		});

		$('div.scroll').html(
			( $(window).width() + 15 ) +' x '+
			$(window).height() + ' Scroll Top: ' + 
			$(window).scrollTop()
		);
		
		$(window).scroll(function() {
			$('div.scroll').html(
				( $(window).width() + 15 ) +' x '+
				$(window).height() + ' Scroll Top: ' + 
				$(window).scrollTop()
			);
		});

		$(window).resize(function() {
			$('div.scroll').html(
				( $(window).width() + 15 ) +' x '+
				$(window).height() + ' Scroll Top: ' + 
				$(window).scrollTop()
			);

			location.reload();
		});

		// Make the Careers page full-height
		$('.careers section').css('min-height', $(window).height() - 177);

		$('.contact section').css('min-height', $(window).height() - 177);


		$('.wpcf7-add-skill-btn').click(function() {
	         //Change the value of the hidden field to the latest number
	         $('input[name="skill_ctr"]').val( $('.skill-row').length );
	        
	         //Get the number of rows
	         var skill_number = $('.skill-row').length + 1;

	         // Clone the first row
	         var skill_element = $('.skill-row').first().clone(true);

	         // Change its incrementing name 
	         $(skill_element).find('[name="skills-1"]').attr('name', 'skills-' + skill_number).val('');
	         $(skill_element).find('[name="score-1"]').attr('name', 'score-' + skill_number).val('1');
	         $(skill_element).find('[name="years_of_exp-1"]').attr('name', 'years_of_exp-' + skill_number).val('1');

	         //Change the value of the hidden field to the latest number
	         $('input[name="skill_ctr"]').val(skill_number);

	         // Append the row
	         $('#sample-table-1 tbody').append(skill_element);
	    });

	    $('.skill-list li').on('hover', function() {
	    	$(this).find('ul').collapse('toggle');
	    });

	});
	
})(jQuery);