(function($) {
	$(window).ready(function() {
		var wHeight = $(window).height(),
			wWidth = $(window).width(),
			frame1Stop = wHeight - 100,
			frame1go = frame1Stop + 500,
			frame2start = frame1go,
			frame2stop = frame1go + wWidth + 20,
			frame2list1_go = frame2stop + 500,
			frame2list2_start = frame2list1_go,
			frame2go = frame2list2_start + 500,
			frame3start = frame2go,
			frame3stop = frame3start + wHeight - 100,
			frame3go = frame3stop + 500,
			frame4start = frame3go + 100,
			frame4stop = frame3go + wHeight,
			frame4list_1 = frame4stop + 300,
			frame4list_2 = frame4list_1 + 300,
			frame4list_3 = frame4list_2 + 300,
			frame4list_a = frame4list_3 + 300,
			frame4go = frame4list_a + 500,
			footerstart = frame4go + 100,
			footerstop = footerstart + wHeight- 100;

			// For Section 4 & 5
			// frame5start = frame4go + 100,
			// frame5stop = frame5start + wHeight - 100,
			// frame5go = frame5stop + 500,
			// frame6start = frame5go - 200,
			// frame6stop = frame6start + wHeight + 160,
			// footerstart = frame6start + ( wWidth / 2 ),
			// footerstop = footerstart + 100;

		$('.wrapper').parallaxBackground({ left: 0.010, top: 0.010, start: 0 });

		$('.index article').css('height', ( wHeight - 97 ) );

		$('.box-content').css({
			'top': wHeight, 
			'height': wHeight 
		})
		.parallaxPosition(
			{ left: 0, top: -1, start: 0 },
			{ left: 0, top: 0, start: wHeight - 97 }
		);

		$('.frame-1').css('top', wHeight + 60 )
		.parallaxPosition(
			{ left: 0, top: -1, start: 0 },
			{ left: 0, top: 0, start: frame1Stop },
			{ left: -1, top: 0, start: frame1go }
		);

		$('.frame-2').css({ 
			'top': 162,
			'left': wWidth + 15
		})
		.parallaxPosition(
			{ left: -1, top: 0, start: frame2start },
			{ left: 0, top: 0, start: frame2stop },
			{ left: 0, top: -1, start: frame2go }
		);

		$('.skill-list-1').parallaxOpacity(
			{ opacity: -.005, start: frame2list1_go }
		);
		$('.skill-list-2').parallaxOpacity(
			{ opacity: .005, start: frame2list2_start }
		);

		$('.frame-3').css( 'top', wHeight + 60 )
		.parallaxPosition(
			{ left: 0, top: -1, start: frame3start },
			{ left: 0, top: 0, start: frame3stop },
			{ left: 0, top: -1, start: frame3go }
		);

		$('.drop-cont').parallaxOpacity({ opacity: -.010, start: frame3go });

		$('.frame-4').css( 'top', wHeight + 60 )
		.parallaxBackground({ left: 0.05, top: 0, start: frame3stop })
		.parallaxPosition( 
			{ left: 0, top: -1, start: frame4start },
			{ left: 0, top: 0, start: frame4stop + 65 },
			{ left: 0, top: -1, start: frame4go }
		);

		$('.frame-4 .frame-4-list-1').parallaxOpacity({ opacity: .005, start: frame4list_1 });
		$('.frame-4 .frame-4-list-2').parallaxOpacity({ opacity: .005, start: frame4list_2 });
		$('.frame-4 .frame-4-list-3').parallaxOpacity({ opacity: .005, start: frame4list_3 });
		$('.frame-4 a').parallaxOpacity({ opacity: .005, start: frame4list_a });

		// $('.frame-5').css('top', wHeight + 60 )
		// .parallaxPosition(
		// 	{ left: 0, top: -1, start: frame5start },
		// 	{ left: 0, top: 0, start: frame5stop },
		// 	{ left: 0, top: -1, start: frame5go }
		// );

		// $('.frame-6').css('top', -(wHeight) )
		// .parallaxPosition(
		// 	{ left: 0, top: 1, start: frame6start },
		// 	{ left: 0, top: 0, start: frame6stop }
		// );

		$('footer').css( 'top', wHeight )
		.parallaxPosition(
			{ left: 0, top: -1, start: footerstart },
			{ left: 0, top: 0, start: footerstop }
		);

		// to scroll to first frame using the homepage button
		$('.index .scroll-btn').click(function(e) {
			e.preventDefault;
			$('#frame-1').scrollView();
		});

		$.fn.scrollView = function () {
		  return this.each(function () {
		    $('html, body').animate({
		      scrollTop: $(this).offset().top - 60
		    }, 1000);
		  });
		}

	$('.index').css('height', footerstop * 1.2);

	});
	
})(jQuery);