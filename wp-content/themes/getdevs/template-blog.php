<?php 

/* Template Name: Blog */ 

get_header(); ?>


    <main role="main">
        <section class="blog block">
            <article>

                <?php //$meta = get_post_meta( get_the_ID(), 'Top Icon'); 
                    //if($meta[0]) : ?>

                        <!-- <div class="top-icon center text-center">
                            <img src="<?php //echo $meta[0]; ?>" />
                        </div> -->
                    
                <?php //endif; ?>

                <div class="center bg-white clearfix">

                    <div class="entry with-sidebar">
                        <?php 

                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array(
                        'orderby' => 'date',
                            'order' => 'DESC',
                            'posts_per_page' => 5,
                            'paged' => $paged
                            );
                        query_posts($args);

                        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                            
                        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
                            <div class="calendar">
                                <?php 
                                    $blog_day   = get_the_time('j', $post->ID); 
                                    $blog_month = get_the_time('M', $post->ID); 

                                    echo '<div class="month">'. $blog_month .'</div>';
                                    echo '<div class="day">'. $blog_day .'</div>';
                                ?>
                            </div>

                            <h1>
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                </a>
                            </h1>
                            <div class="featured text-center">
                                <?php 
                                    // check if the post has a Post Thumbnail assigned to it.
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail();
                                    }
                                ?>
                            </div>

                            <div class="content">
                                <p><?php the_excerpt(); ?></p>
                            </div>

                    <!-- <div class="meta">
                                <div>
                                    <i class="calendar-icon"></i>
                                    <?php the_date(); ?>
                                </div>

                                <div>
                                    <i class="comment-icon"></i>
                                    <a href="<?php the_permalink(); ?>"><?php comments_number( 'no comment', 'one comment', '% comments' ); ?></a>
                                </div>
                            </div> -->
                        </div>
                            
                        <?php endwhile; else: ?>

                        <article>
                            <h2>Sorry, nothing to display.</h2>
                        </article>

                        <?php endif; ?>

                        <?php get_template_part('pagination'); ?>
                    </div>
                    
                    <?php get_sidebar(); ?>
                </div>
            </article>
        </section> <!-- /section -->
    </main>
<?php get_footer(); ?>