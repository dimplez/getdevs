<?php get_header(); ?>

	<main role="main">
	<!-- section -->
	<section class="block">

	

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="center bg-white">
				<div class="single-cont clearfix">
					<div class="calendar">
                        <?php 
                            $blog_day   = get_the_time('j', $post->ID); 
                            $blog_month = get_the_time('M', $post->ID); 

                            echo '<div class="month">'. $blog_month .'</div>';
                            echo '<div class="day">'. $blog_day .'</div>';
                        ?>
                    </div>
					<div class="entry with-sidebar">
						<?php if (have_posts()): while (have_posts()) : the_post(); ?>


						<h1><?php the_title(); ?></h1>

						<!-- post thumbnail -->
						<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail(); // Fullsize image for the single post ?>
							</a>
						<?php endif; ?>
						<!-- /post thumbnail -->

						<!-- post details -->
						<!-- <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
						<span class="author"><?php _e( 'Published by', 'html5blank' ); ?> <?php the_author_posts_link(); ?></span>
						<span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' )); ?></span> -->
						<!-- /post details -->

						<?php the_content(); // Dynamic Content ?>

						<!-- <hr /> -->

						<?php the_tags( __( '<strong>Tags:</strong> ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>

						<?php //_e( '<strong>Filed under:</strong> ', 'html5blank' ); the_category(', '); // Separated by commas ?>

						<!-- <p><?php _e( 'This post was written by ', 'html5blank' ); the_author(); ?></p> -->

						<?php //edit_post_link(); // Always handy to have Edit Post Links available ?>
						<!-- <hr /> -->

						<?php comments_template(); ?>

						<?php endwhile; ?>

						<?php else: ?>

							<!-- article -->
							<article>

								<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

							</article>
							<!-- /article -->

						<?php endif; ?>
					</div>

					<?php get_sidebar(); ?>
				</div>
			</div>
		</article>
		<!-- /article -->

	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>
