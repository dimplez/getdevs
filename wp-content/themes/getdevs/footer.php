            <!-- footer -->
            <footer class="footer block" role="contentinfo">
                <div class="center">
                    <div class="row">
                        <div class="col-md-4 col-xs-6">
                            <h1>Pages</h1>
                            <?php extra_nav(); ?>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <h1>Latest Post</h1>

                            <?php $the_query = new WP_Query( 'showposts=1' ); 
                                while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                                
                                <a href="<?php the_permalink() ?>" class="footer-title"><?php the_title(); ?></a>
                                <small><em><?php the_date();?></em></small>

                                <p class="text-left"><?php echo footer_excerpt(30); ?></p>
                                
                                <a href="<?php the_permalink(); ?>">Read More &raquo;</a>
                            <?php endwhile;?>
                        </div>
                        <div class="col-md-4 col-xs-12 text-left">
                            <a href="<?php echo home_url(); ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Logo" class="logo-img center-block">
                            </a>

                            <div class="address-cont">
                                <address>
                                    <strong>Get Devs, Inc.</strong><br>
                                    25 Broadway<br>
                                    New York<br>
                                    NY 10004<br>
                                    <abbr title="Phone">P:</abbr> 646-634-9235
                                </address>
                                <ul class="footer-social">
                                    <li>
                                        <a href="https://www.facebook.com/getdevs" target="_blank"><i class="icon-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/GetDevs" target="_blank"><i class="icon-twitter"></i></a>
                                    </li>
                                    <!-- <li>
                                        <a href="#" target="_blank"><i class="icon-google"></i></a>
                                    </li> -->
                                    <li>
                                        <a href="https://www.linkedin.com/company/getdevs" target="_blank"><i class="icon-linkedin"></i></a>
                                    </li>
                                    <li>
                                        <a href="mailto:cpalmer@getdevs.com" target="_blank"><i class="icon-email"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row text-center">
                        <div class="col-md-12">
                            <p class="copyright">
                                Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved.
                            </p>
                        </div>
                    </div>

                    <!-- <div class="pull-left">
                        
                        <p class="copyright">
                            Copyright &copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>. All Rights Reserved.
                        </p>
                        
                        <?php //extra_nav(); ?>
                    </div> -->

                    <!-- <ul class="footer-social pull-right">
                        <li>
                            <a href="https://www.facebook.com/getdevs" target="_blank"><i class="icon-facebook"></i></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/GetDevs" target="_blank"><i class="icon-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" target="_blank"><i class="icon-google"></i></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/getdevs" target="_blank"><i class="icon-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="mailto:cpalmer@getdevs.com" target="_blank"><i class="icon-email"></i></a>
                        </li>
                    </ul> -->
                </div>
            </footer>
            <!-- /footer -->

        </div>
        <!-- /wrapper -->
        
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
        <?php wp_footer(); ?>

        <!-- analytics -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-39848746-1', 'auto');
          ga('send', 'pageview');

        </script>
    </body>
</html>
