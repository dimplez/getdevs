<?php get_header(); ?>
	
	<!-- URL: website.com/skills/{term}-->
	<main role="main">
		<section class="block">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="center"> 
					<div class="entry">
						
		     		<?php $current_term = get_term_by('slug', $wp_query->query_vars['skills'], 'skills');

		     		// Get the Users under this Parent Category by slug
					$candidates = get_posts( 
						array (
							'showposts' => 3,
						    'post_type' => 'resume_portal',
						    'tax_query' => array (
						        array(
							        'taxonomy' => 'skills',
							        'field' => 'slug',
							        'terms' => $wp_query->query_vars['skills']
							    )
						    )
						)
					);
					if( ! empty($candidates) ) : ?>

						<h1><?php echo $current_term->name; ?> Candidates</h1>
						<table class="table table-hover table-condensed table-striped table-bordered">
							<thead>
						        <tr>
						        	<th class="text-center">Profile</th>
						        	<th class="text-center"> Top Skills </th>
						          	<th class="text-center"> Years of Experience </th>
						          	<th class="text-center"> # </th>
					        	</tr>
					     	</thead>
					     	<tbody>
							<?php foreach ($candidates as $candidate) : 

							// Candidate info
			     			$info = resume_portal_get_info_id( $candidate->ID );

			     			// get the skills in the DB
			     			$skills = resume_portal_get_skills_id( $candidate->ID );

			     			// json decode it
							$skill = json_decode( $skills['skills'], true); 

							//only the published will show
							if($info['published'] == 'Y') : ?>
					     		<tr class="text-center">
					     			<td>
					     				<a href="<?php echo $candidate->guid; ?>" class="btn btn-primary btn-sm">
					     					<span class="glyphicon glyphicon-user"></span>
					     				</a>
					     			</td>
					     			<td>
					     			<?php 
					     			$skill = array_slice($skill, 0, 3);
					     			foreach ($skill as $key => $value) { 
				     				// for random class color
									$colors = array(
										1 => 'default', 
										2 => 'primary', 
										3 => 'success', 
										4 => 'info', 
										5 => 'warning', 
										6 => 'danger'
									);
									$new_color = array_rand($colors, 1);
									echo '<a class="label label-'. $colors[$new_color] .'" href="/skills/'. $value['skill'] .'">';
									echo $value['skill'];
									echo '</a> ';
					     			}
					     			?>
					     			</td>
					     			<td><?php echo $value['yrs_exp']; ?></td>
					     			<td><?php echo $info['c_number'] ?></td>
					     		</tr>
					     	<?php endif; endforeach; ?>

							</tbody>
						</table>

					<?php else : ?>
						<h1 class="text-center">Sorry, nothing to display.</h1>
					<?php endif; ?>

					</div>
				</div>
		</section>
	</main>

<?php get_footer(); ?>