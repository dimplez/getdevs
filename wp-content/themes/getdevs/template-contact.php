<?php 

/* Template Name: Contact Us */ 

get_header(); ?>

<main role="main">
		<section class="block">

			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="center">
                    		<h1 class="title text-center">We usually respond within the hour.</h1>
                        	<div class="with-sidebar"> 

                    		<?php 
                    		if( isset($_POST['submit']) ) { 
								$to = 'cpalmer@getdevs.com';
								$subject = 'From Contact Page';
								$message = $_POST['contact-message'];
								$headers = array('Content-Type: text/html; charset=UTF-8', 'From: '. $_POST['contact-name'] .' <'. $_POST['contact-email'] .'>');

								wp_mail( $to, $subject, $message, $headers );
							}
                    		?>

                    		<form role="form" method="POST" action="">
								<?php the_content(); ?>
								<div class="captcha-cont row">
								  	<div class="col-xs-2">
								    	<input id="num1" name="num1" type="text" class="form-control text-center" value="<?php echo rand( 0, 5 ) ?>" readonly>
								  	</div>
								  	<div class="col-xs-1 text-center"> + </div>
								  	<div class="col-xs-2">
								    	<input id="num2" name="num2" type="text" class="form-control text-center" value="<?php echo rand( 6, 9 ) ?>" readonly>
								  	</div>
								  	<div class="col-xs-1 text-center"> = </div>
								  	<div class="col-xs-3">
								    	<input id="captcha" name="captcha" maxlength="2" type="number" min="0" max="20" class="form-control text-center" placeholder="?" required>
								  	</div>
								  	<div class="col-xs-3">
										<input type="submit" name="submit" class="btn btn-primary btn-block" value="Submit" />
									</div>
									<div class="col-xs-12">
								  		<br />
								  		<div class="captcha-alert alert alert-danger hide" role="alert">
		  									<strong><span class="alert-link">Wrong Captcha!</span></strong>
										</div>
									</div>
								</div>
							</form>
                        </div>

                        <div class="sidebar text-right">
							<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('contact-widget-area')) ?>
                        </div><!-- .sidebar-->

					</div>
				</article>

			<?php endwhile; else: ?>

				<article>
					<div class="entry"> 
                        <div class="center">
							<h2><?php _e( 'Sorry, nothing to display.' ); ?></h2>
						</div>
					</div>
				</article>

			<?php endif; ?>

		</section>
	</main>
<?php get_footer(); ?>