<?php /* Template Name: Demo Page Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="block">
			<div class="center bg-white clearfix"> 
				<div class="entry with-sidebar">
					<h1>Demo Page</h1>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
