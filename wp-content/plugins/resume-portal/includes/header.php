<?php $dir_url = plugins_url('resume-portal'); ?>

<!-- basic styles -->
<link rel="stylesheet" href="<?php echo $dir_url; ?>/assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo $dir_url; ?>/assets/css/font-awesome.min.css" />

<!-- fonts -->
<link rel="stylesheet" href="<?php echo $dir_url; ?>/assets/css/ace-fonts.css" />

<!-- page specific plugin styles -->
<link rel="stylesheet" href="<?php echo $dir_url; ?>/assets/css/jquery-ui-1.10.3.custom.min.css" />

<!-- ace styles -->
<link rel="stylesheet" href="<?php echo $dir_url; ?>/assets/css/ace.min.css" />
<link rel="stylesheet" href="<?php echo $dir_url; ?>/assets/css/ace-rtl.min.css" />
<link rel="stylesheet" href="<?php echo $dir_url; ?>/assets/css/ace-skins.min.css" />

<link rel="stylesheet" href="<?php echo $dir_url; ?>/assets/css/custom.css" />

<!-- ace settings handler -->
<script src="<?php echo $dir_url; ?>/assets/js/ace-extra.min.js"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="<?php echo $dir_url; ?>/assets/js/html5shiv.js"></script>
<script src="<?php echo $dir_url; ?>/assets/js/respond.min.js"></script>
<![endif]-->

<?php

// Add an nonce field so we can check for it later.
wp_nonce_field( 'resume_portal_metabox', 'resume_portal_metabox_nonce' );

// load the needed default value 
$info = array(
    'yrs_exp' 		=> '5',
    'salary' 		=> '8000',
);

// Get the saved informations based on the ID
if(resume_portal_get_info_id( get_the_ID() )) {
	// Put the Result in a variable
	$info = resume_portal_get_info_id( get_the_ID() );
}

// Count all the Users
$user_count = resume_portal_get_all_user();

// Decode the Skill into Array
$skills = json_decode($info['skills'], true);

?>