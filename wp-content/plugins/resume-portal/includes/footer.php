<!-- basic scripts -->
<script src="<?php echo $dir_url; ?>/assets/js/jquery-2.0.3.min.js"></script>
<script src="<?php echo $dir_url; ?>/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?php echo $dir_url; ?>/assets/js/jquery.knob.min.js"></script>
<script src="<?php echo $dir_url; ?>/assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="<?php echo $dir_url; ?>/assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo $dir_url; ?>/assets/js/jquery.autosize.min.js"></script>


<!--[if !IE]> -->
<script type="text/javascript">
	window.jQuery || document.write("<script src='<?php echo $dir_url; ?>/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>
<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='<?php echo $dir_url; ?>/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
	if("ontouchend" in document) document.write("<script src='<?php echo $dir_url; ?>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?php echo $dir_url; ?>/assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->
<!--[if lte IE 8]>
  <script src="<?php echo $dir_url; ?>/assets/js/excanvas.min.js"></script>
<![endif]-->

<!-- ace scripts -->
<script src="<?php echo $dir_url; ?>/assets/js/ace-elements.min.js"></script>
<script src="<?php echo $dir_url; ?>/assets/js/ace.min.js"></script>
<script src="<?php echo $dir_url; ?>/assets/js/ace-extra.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			//Change the value of the hidden field to the latest number
			$('input[name="skill_ctr"]').val( $('.skill-row').length );

			// Delete row script
			$(':not(.remove-row-1).delete-btn').click(function(e) {
				//Change the value of the hidden field to the latest number
				var reduced_skill_number = $('input[name="skill_ctr"]').val( $('.skill-row').length - 1 );

				/* if row number is greater than one
				* to prevent deleting all row
				*/
				if(reduced_skill_number.val() > 1) {
					$('input[name="skill_ctr"]').val( $('.skill-row').length - 1 );
					$(this).closest( ".skill-row" ).remove();
				}
				e.preventDefault();
			});

			$('.remove-row-1').click(function(e) { 
				e.preventDefault(); 
				$(this).closest( ".skill-row" ).remove();
			});

			// // Add Skill Row script
			$('.add-skill-btn').click(function() {
				//Change the value of the hidden field to the latest number
				$('input[name="skill_ctr"]').val( $('.skill-row').length );
				
				//Get the number of rows
				var skill_number = $('.skill-row').length + 1;

				// Clone the first row
				var skill_element = $('.skill-row').first().clone(true);

				// remove the delete icon when cloned
				// $(skill_element).find('a').remove();

				// Change its incrementing name 
				$(skill_element).find('[name="skills-1"]').attr('name', 'skills-' + skill_number).val('');
				$(skill_element).find('[name="score-1"]').attr('name', 'score-' + skill_number).val('1');
				$(skill_element).find('[name="years_of_exp-1"]').attr('name', 'years_of_exp-' + skill_number).val('1');
				$(skill_element).find('.remove-row-1').attr('class', 'remove-row-' + skill_number + ' red pull-right delete-btn');

				//Change the value of the hidden field to the latest number
				$('input[name="skill_ctr"]').val(skill_number);

				// Append the row
				$('#sample-table-1 tbody').append(skill_element);
			});

			

			$(".knob").knob();

		    $('.date-picker').datepicker({
		    	startView: 1,
		    	minViewMode: 1,
		    	autoclose: true
		    })
		    .next()
		    .on(ace.click_event, function() {
				$(this).prev().focus();
			});

			$('.input-mask-phone').mask('(9999)-999-9999');

			$('#resume_upload').ace_file_input({
				no_file: 'No File ...',
				btn_choose: 'Choose',
				btn_change: 'Change',
				droppable: true,
				onchange: null,
				thumbnail: true //| true | large
				//whitelist:'doc|docx|pdf|',
				//blacklist:'exe|php|html|js|'
				//onchange:''
				//
			});

			// Delete uploaded resume
			$('.delete_resume').click(function(e) {
				e.preventDefault();

				// change the hidden input to 0
				$('[name="resume_file"]').val(0);

				// remove the resume link
				$('.resume_name').html('');
			});

			// Code Sample textarea autosize
			$('.code-sample textarea[class*=autosize]').autosize({append: "\n"});

		});
	})(jQuery);
</script>