<?php include('header.php'); ?>

<div class="rp-metabox form-horizontal wrap">
    
    <!-- Main -->
	<section>
		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right"> Published? </label>

			<div class="col-sm-9">
				<label>
					<input name="published" class="ace ace-switch ace-switch-5" type="checkbox" 
					<?php echo $info['published'] == 'Y' ? 'checked' : '' ?> />
					<span class="lbl"></span>
				</label>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="c_number"> Candidate Number </label>

			<div class="col-sm-9">
				<?php $min_max_val = $info['c_number'] ? $info['c_number'] : $user_count + 1; ?>

				<input type="number" id="c_number" name="c_number" min="<?php echo $min_max_val; ?>" max="<?php echo $min_max_val; ?>"
				value="<?php echo $min_max_val; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="c_position"> Position/Title </label>

			<div class="col-sm-9">
				<input name="c_position" id="c_position" type="text" placeholder="Position/Title" class="col-xs-12 col-sm-12" 
				value="<?php echo $info['c_position']; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right"> Years of Relevant Industry Experience </label>

			<div class="col-sm-9">

				<div class="knob-container inline">
					<input type="text" class="input-small knob" data-min="0" data-max="100" data-step="1" 
					data-width="100" data-height="100" data-thickness=".3" name="yrs_exp" value="<?php echo $info['yrs_exp']; ?>" />
				</div>
			</div>
		</div>
	</section>

	<div class="hr hr-18 dotted hr-double"></div>

	<!-- Educational Background -->
	<section class="educ-bg">
		<div class="col-xs-12 label label-lg label-info arrowed-in arrowed-right title-banner">
			<b>Educational Background</b>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="educ"> Education </label>

			<div class="col-sm-9">
				<input name="education" id="educ" type="text" placeholder="Enter School Name" class="col-xs-12 col-sm-12"
				value="<?php echo $info['education']; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="degree"> Degree </label>

			<div class="col-sm-9">
				<input name="degree" id="degree" type="text" placeholder="Enter Degree" class="col-xs-12 col-sm-12" 
				value="<?php echo $info['degree']; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="yrs_grad"> Year </label>

			<div class="col-sm-9">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="input-group">
							<input class="form-control date-picker" id="yrs_grad" type="text" data-date-format="yyyy-mm-dd" 
							placeholder="Enter Year of Graduation" name="yrs_grad" value="<?php echo $info['yrs_grad']; ?>"/>
							<span class="input-group-addon">
								<i class="icon-calendar bigger-110"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="hr hr-18 dotted hr-double"></div>

	<!-- Top Skills -->
	<section class="top-skills">
		<div class="col-xs-12 label label-lg label-info arrowed-in arrowed-right title-banner">
			<b>Top Skills </b><small>(based on marketability)</small>
		</div>
		<div class="table-responsive">
			<input type="hidden" name="skill_ctr" />
			<table id="sample-table-1" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th class="text-center">Skills</th>
						<th class="text-center">Self Assessment</th>
						<th class="text-center">Years of Experience</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						// for counter
						$j = 1;

						// if it has saved info
						if($skills) {

							// $c = call_user_func_array(array_merge, $skills);
							// list each one
							foreach ($skills as $key) {
								echo resume_portal_add_skill($key, $j);
								$j++;
							}
						} else {
							// default row
							echo resume_portal_default_row();
						}
					?>
				</tbody>
			</table>
		</div>
		<button type="button" class="btn btn-primary add-skill-btn">
			<i class="icon-plus align-top bigger-125"></i>
			Add Skill
		</button>
	</section>

	<div class="hr hr-18 dotted hr-double"></div>

	<!-- Private Notes -->
	<section class="private-notes">
		<div class="col-xs-12 label label-lg label-info arrowed-in arrowed-right title-banner">
			<b> Private Notes </b>
		</div>
        
        <div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="location"> Location </label>

			<div class="col-sm-9">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="icon-home"></i>
							</span>
							<input name="location" id="location" type="text" placeholder="Location" class="col-xs-12 col-sm-12" 
							value="<?php echo $info['location']; ?>" />
						</div>
					</div>
				</div>
			</div>
		</div>
        
		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="email"> Email </label>

			<div class="col-sm-9">
				<div class="input-group">
					<span class="input-group-addon">
						<i class="icon-envelope"></i>
					</span>

					<input name="email" id="email" type="email" placeholder="Email" class="col-xs-12 col-sm-12" 
					value="<?php echo $info['email']; ?>" />
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="avail"> Availability </label>

			<div class="col-sm-9">
				<div class="input-group">
					<span class="input-group-addon">
						<i class="icon-bookmark"></i>
					</span>
					<input name="availability" id="avail" type="text" placeholder="Availability" class="col-xs-12 col-sm-12"
					value="<?php echo $info['availability']; ?>" />
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="salary"> Salary Requirement </label>

			<div class="col-sm-9">
				<div class="input-group">
					<span class="input-group-addon">
						<i class="icon-dollar"></i>
					</span>
					<input name="salary" id="salary" type="number" placeholder="Salary Requirement" class="col-xs-12 col-sm-12" min="8000" step="500"
                    value="<?php echo $info['salary']; ?>" />
				</div>
			</div>
		</div>
        
        <div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="expected_salary"> Expected Salary </label>

			<div class="col-sm-9">
				<div class="input-group">
					<span class="input-group-addon">
						<i class="icon-dollar"></i>
					</span>
					<input name="expected_salary" id="expected_salary" type="number" placeholder="Expected Salary" class="col-xs-12 col-sm-12" 
                    min="0" step="500" value="<?php echo $info['expected_salary']; ?>" />
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="phone"> Phone Number </label>

			<div class="col-sm-9">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="icon-phone"></i>
							</span>
							<input name="phone" class="form-control input-mask-phone" type="text" id="form-field-mask-2" placeholder="Phone Number"
							value="<?php echo $info['phone']; ?>" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="hr hr-18 dotted hr-double"></div>

	<section class="code-sample">
		<div class="col-xs-12 label label-lg label-info arrowed-in arrowed-right title-banner">
			<b> Notice </b>
		</div>

		<textarea name="notice" class="autosize-transition form-control" rows="10"><?php echo $info['notice']; ?></textarea>
	</section>
    
	<br />
    
	<section class="code-sample">
		<div class="col-xs-12 label label-lg label-info arrowed-in arrowed-right title-banner">
			<b> Code Sample </b>
		</div>

		<textarea name="code_sample" class="autosize-transition form-control" rows="10"><?php echo stripslashes( $info['code_sample'] ); ?></textarea>
	</section>

	<!--  Action Buttons -->
	<div class="clearfix form-actions">
		<div class="col-md-offset-3 col-md-9">
			<button class="btn btn-info btn-block" type="reset">
				<i class="icon-undo bigger-110"></i>
				Reset
			</button>
		</div>
	</div>
</div>

<?php include('footer.php'); ?>