<?php 

/**
 * Plugin Name: Resume Portal
 * Plugin URI: http://getdevs.com
 * Description: A brief description of the Plugin.
 * Version: 1.0
 * Author: James Yanzon
 * Author URI: http://getdevs.com
 * License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// If accessed directly, kill it!
defined('ABSPATH') or die("No script kiddies please!");

//Activation Hook
function resume_portal_activation() {
    global $wpdb;

    //DB Name
    $tablename = $wpdb->prefix .'resumeportal';

    /*
     * Set the default character set and collation for this table.
     * If we don't do this, some characters could end up being converted 
     * to just ?'s when saved in our table.
     */
    $charset_collate = '';

    if ( ! empty( $wpdb->charset ) ) {
        $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
    }

    if ( ! empty( $wpdb->collate ) ) {
        $charset_collate .= " COLLATE {$wpdb->collate}";
    }

    // Create Table Command
    $sql = "CREATE TABLE $tablename (
        id mediumint DEFAULT '0' NULL,
        c_name tinytext NULL,
        published char(1) DEFAULT 'N' NULL,
        c_number mediumint NOT NULL,
        c_position tinytext NULL, 
        yrs_exp tinyint NULL,
        education tinytext NULL,
        degree tinytext NULL,
        yrs_grad DATE NULL,
        email tinytext NULL,
        phone varchar(20) NULL,
        availability tinytext NULL,
		location tinytext NULL,
        salary mediumint NULL,
		expected_salary mediumint NULL,
		notice mediumtext NULL,
        code_sample mediumtext NULL,
        skills blob NULL,
        PRIMARY KEY ( id )
    ) $charset_collate;";

    // Include WOrdpress Script
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    // Run it!
    dbDelta( $sql );
    
    /* 
     *
     * Rewrite the Permalinks 
     *
     */
    // Candidate Post Type
    reg_custom_post();

    //Skill Taxonomy
    skills_category();

    // The Rewrite Function
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'resume_portal_activation' );

// Rewrite the Permalink when switching theme
function rewrite_flush_switch_theme() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'rewrite_flush_switch_theme' );

//Deactivation Hook
function resume_portal_deactivation() {
    global $wpdb;

    //DB Name
    $tablename = $wpdb->prefix .'resumeportal';

    // Drop DB Command
    $sql = "DROP TABLE IF EXISTS $tablename;";

    // Run it!
    $wpdb->query($sql);
}
register_uninstall_hook( __FILE__, 'resume_portal_deactivation');

/**
 * Register a Resume Portal post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
if ( ! function_exists('reg_custom_post') ) {

    // Register Custom Post Type
    function reg_custom_post() {

        $labels = array(
            'name'                => 'Candidates',
            'singular_name'       => 'Candidate',
            'menu_name'           => 'Resume Portal',
            'parent_item_colon'   => 'Parent Candidate:',
            'all_items'           => 'All Candidates',
            'view_item'           => 'View Candidate',
            'add_new_item'        => 'Candidate Name',
            'add_new'             => 'Add New',
            'edit_item'           => 'Edit Candidate',
            'update_item'         => 'Update Candidate',
            'search_items'        => 'Search Candidate',
            'not_found'           => 'Not found',
            'not_found_in_trash'  => 'Not found in Trash',
        );
        $args = array(
            'label'               => 'resume_portal',
            'labels'              => $labels,
            'supports'            => array( 'title', 'thumbnail' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => false,
            'rewrite'             => array( 'slug' => 'software-development-team' ),
            'menu_position'       => 5,
            'menu_icon'           => plugins_url('resume-portal'). '/assets/img/resume-portal.ico',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'register_meta_box_cb'=> 'resume_portal_metabox',
            'capability_type'     => 'post'
        );
        register_post_type( 'resume_portal', $args );

    }
    // Hook into the 'init' action
    add_action( 'init', 'reg_custom_post', 0 );
}

// Create Skill Taxonomy
function skills_category() {
    register_taxonomy(
        'skills',
        'resume_portal',
        array(
            'hierarchical' => true,
            'label'        => __( 'Skills' ),
            'rewrite'      => array( 'slug' => 'skills' )
        )
    );
}
add_action( 'init', 'skills_category' );

// Create the Metabox
function resume_portal_metabox() {
    add_meta_box(
        'resume_portal_metaboxID',
        'Candidate Information',
        'resume_portal_metabox_page',
        'resume_portal',
        'normal',
        'high'
    );

    add_meta_box(
        'resume_portal_resume_upload_metaboxID',
        'Upload Resume',
        'resume_portal_resume_upload_metabox_page',
        'resume_portal',
        'side',
        'low'
    );
}

// Get the Template
function resume_portal_metabox_page() {
    include('includes/admin-page.php');
}

// output an input file type
function resume_portal_resume_upload_metabox_page() {
    // nonce field
    wp_nonce_field(plugin_basename(__FILE__), 'resume_portal_attachment_nonce');

    // to check if theres is saved (default 1)
    echo '<input type="hidden" name="resume_file" value="1">';
    echo '<input type="file" id="resume_upload" name="resume_upload" />';

    // get the post meta
    $resume_link = get_post_meta( get_the_ID(), 'resume_upload', true);

    // if theres a value to the post meta
    // show a link and delete button
    if( ! empty( $resume_link ) ) {
        echo '<a href="'. $resume_link['url'] .'" class="resume_name">'. $resume_link['name'] .'</a>';
        echo '<button class="btn btn-xs btn-danger delete_resume">';
        echo '<i class="icon-trash bigger-110"></i>';
        echo 'Delete';
        echo '</button>';
    }
}

//edit the form attr to support file upload
function update_edit_form() {
    echo ' enctype="multipart/form-data" encoding="multipart/form-data"';
}
add_action('post_edit_form_tag', 'update_edit_form');

// Save Action Hook
function resume_portal_save_post( $post_id ) {
    global $wpdb;

    //Table Name
    $tablename = $wpdb->prefix .'resumeportal';

    // verify nonce
    if (!wp_verify_nonce($_POST['resume_portal_metabox_nonce'], 'resume_portal_metabox')) { return $post_id; }
        
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return $post_id; }
        
    // check permissions
    if ('resume_portal' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) { return $post_id; } 
        elseif (!current_user_can('edit_post', $post_id)) { return $post_id; }
    }

    // putting all skills in an array
    $skill = array();
    $wp_skill = array();

    for($l = 1; $l <= 50; $l++) {
        if(isset($_POST['skills-'. $l])) {
            //option value (e.g: Group 1.option1)
            // get only the group name (e.g: Group 1)
            $group = current( explode('.', $_POST['skills-'. $l]) );
            // get only the option name (e.g: option1)
            $c_skill = substr(strrchr($_POST['skills-'. $l], '.'), 1);
            
            // push the skills in the $skill
            array_push($skill, array(
                    'group'     => $group,
                    'skill'     => $c_skill,
                    'score'     => $_POST['score-'. $l],
                    'yrs_exp'   => $_POST['years_of_exp-'. $l] 
                )
            );

            // push the skills in the $wp_array
            array_push( $wp_skill, $c_skill );  
        }
    }

    // Saving the Custom Taxonomy for the wordpress DB
    wp_set_object_terms($post_id, $wp_skill, 'skills');

    // upload resume function
    resume_portal_file_upload($post_id);

    $data = array(
        'id'            	=> $post_id,
        'c_name'        	=> $_POST['post_title'],
        'published'     	=> isset($_POST['published']) ? 'Y' : 'N',
        'c_number'      	=> $_POST['c_number'],
        'c_position'    	=> $_POST['c_position'],
        'yrs_exp'       	=> $_POST['yrs_exp'],
        'education'     	=> $_POST['education'],
        'degree'        	=> $_POST['degree'],
        'yrs_grad'      	=> $_POST['yrs_grad'],
        'email'         	=> $_POST['email'],
        'availability'  	=> $_POST['availability'],
		'location'			=> $_POST['location'],
        'salary'        	=> $_POST['salary'],
		'expected_salary' 	=> $_POST['expected_salary'],
        'phone'         	=> $_POST['phone'],
		'notice'         	=> $_POST['notice'],
        'code_sample'   	=> htmlentities( $_POST['code_sample'] ),
        'skills'        	=> json_encode($skill)
    );
    $format = array( '%d', '%s', '%s', '%d', '%s', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%s', '%s', '%s', '%s');

    // Insert Query
    $wpdb->replace($tablename, $data, $format);
}
add_action('save_post_resume_portal', 'resume_portal_save_post');

// Delete Action Hook
function resume_portal_delete_post( $post_id ) {
    global $wpdb;

    //Table Name
    $tablename = $wpdb->prefix .'resumeportal';

    $wpdb->delete($tablename, array('id' => $post_id), array('%d') );
}
add_action( 'before_delete_post', 'resume_portal_delete_post' );

// upload resume function
function resume_portal_file_upload( $post_id ) {
    //security verification
    if(!wp_verify_nonce($_POST['resume_portal_attachment_nonce'], plugin_basename(__FILE__))) {
      return $post_id;
    } 

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
      return $post_id;
    }

    if('resume_portal' == $_POST['post_type']) {
      if(!current_user_can('edit_page', $post_id)) {
        return $post_id;
      } // end if
    } else {
        if(!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } 

    // Make sure the file array isn't empty
    if(!empty($_FILES['resume_upload']['name'])) {
        // Setup the array of supported file types.
        $supported_types = array(
            'application/pdf', //pdf
            'application/msword', // doc
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' // docx
        );

        // Get the file type of the upload
        $arr_file_type = wp_check_filetype(basename($_FILES['resume_upload']['name']));
        $uploaded_type = $arr_file_type['type'];

        // Check if the type is supported. If not, throw an error.
        if(in_array($uploaded_type, $supported_types)) {

            // Use the WordPress API to upload the file
            $upload = wp_upload_bits( $_FILES['resume_upload']['name'], null, file_get_contents($_FILES['resume_upload']['tmp_name']) );
            
            array_push($upload, $upload['name'] = $_FILES['resume_upload']['name']);

            if(isset($upload['error']) && $upload['error'] != 0) {
                wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
            }
            else {
                update_post_meta($post_id, 'resume_upload', $upload);
            }
        } else {
            wp_die("The file type that you've uploaded is not supported.");
        }
    }

    // if the hidden input is equal to 0
    if( $_POST['resume_file'] == 0 ) {
        // get the post meta of the resume_upload
        $resume_link = get_post_meta( $post_id, 'resume_upload', true);

        // delete the post meta
        delete_post_meta($post_id, 'resume_upload');

        // delete the resume file
        unlink($resume_link['file']);
    }
}

// Function for Getting all the info based on ID
function resume_portal_get_info_id( $id ) {
    global $wpdb;

    //Table Name
    $tablename = $wpdb->prefix .'resumeportal';

    // Query the ID
    $info = $wpdb->get_row("SELECT * FROM $tablename WHERE id = $id", ARRAY_A);

    // Return
    return $info;
}

function resume_portal_get_skills_decoded( $id, $array ) {
    global $wpdb;

    //Table Name
    $tablename = $wpdb->prefix .'resumeportal';

    // Query the ID
    $skills = $wpdb->get_row("SELECT * FROM $tablename WHERE id = $id", ARRAY_A);

    // if the second parameter is true(array)
    if($array) {
        // Decode
        $skills =  json_decode( $skills['skills'], true );
        return $skills;
    } 
    // else return it as object
    // Decode
    $skills =  json_decode( $skills['skills'] );

    // Return
    return $skills;
}

// Getting SKills using the ID
function resume_portal_get_skills_id( $id ) {
    global $wpdb;

    //Table Name
    $tablename = $wpdb->prefix .'resumeportal';

    $skills = $wpdb->get_row("SELECT skills FROM $tablename WHERE id = $id", ARRAY_A);

    return $skills;
}

// Getting all users
function resume_portal_get_all_user() {
    global $wpdb;

    //Table Name
    $tablename = $wpdb->prefix .'resumeportal';

    // Query to count all rows
    $user_count = $wpdb->get_var('SELECT COUNT(*) FROM '. $tablename);

    // Return
    return $user_count;
}

// Sort Skill Descending based on score
function resume_portal_skill_sort_score_desc($a, $b) {
    $ascore = intval($a['score']);
    $bscore = intval($b['score']);
    
    if ($ascore == $bscore) {
        return 0;
    }
    return ($ascore < $bscore) ? 1 : -1;    
}

// Request Resume Mail function
function resume_portal_request_file($file) {
    //if submit button is set
    if( isset($_POST['request-btn']) ) {
        $to      = $_POST['request-email']; //$_POST['request-name'] .' <'. $_POST['request-email'] .'>'
        $subject = 'Resume Request';
        $message = 'Candidate Name: '. get_the_title() .'<br />';
        $message .= 'Link: <a href="'. get_permalink() .'">'. get_permalink() .'</a><br /><br />';
        $message .= 'Requested by: '. $_POST['request-name'] .'<br />';
        $message .= 'Company: '. $_POST['request-company'] .'<br>';
        $message .= 'Email: '. $_POST['request-email'] .'<br>';
        $message .= 'Phone#: '. $_POST['request-phone'];
        $headers = array('Content-Type: text/html; charset=UTF-8', 
            'From: Charles Palmer <cpalmer@getdevs.com>', 
            'Cc: Charles Palmer <cpalmer@getdevs.com>',
            'Cc: James Yanzon <yanzon@getdevs.com>'
        );
        
        // send it
        if(wp_mail( $to, $subject, $message, $headers, $file['file'] )) {
            echo '<h1 class="text-center">Request Sent to '. $_POST['request-email'] .'!</h1>';
        } else {
            echo '<h1 class="text-center">Problem Occured!</h1>';
        }
    }

}

// Shortcode for the Home page to show random Candidates
function resume_portal_homepage_candidate() { ob_start(); ?>
<?php 
        global $wpdb;
        $tablename = $wpdb->prefix . 'resumeportal';

        // Limit to 3
        for( $c = 0; $c <= 2; $c++ ) : 

        // randomize the maximum number of user
        $user_count = rand(1, resume_portal_get_all_user());

        // query depends on the randomed c_number
        $info = $wpdb->get_row("SELECT * FROM $tablename WHERE c_number = $user_count", ARRAY_A);
		
        // get the skills of the candidate
        $skills = resume_portal_get_skills_decoded( $info['id'], true );

        // get the post info based on the id given by $info
        $post_info = get_post($info['id'], ARRAY_A);
		
        // also get the post featured image based on the id given by $info
        $feat_image = wp_get_attachment_url( get_post_thumbnail_id($info['id']) );

        // show only published posts
        if($info['published'] == 'Y' && $post_info['post_status'] == 'publish') : 
        ?>

<div class="candidate thumbnail">
    <?php 
                    if($feat_image) {
                        echo '<img src="'. $feat_image .'" class="attachment-profile-image" height="100" width="100" alt="'. $post_info['post_title'] .'" />';
                    } else {
                        echo '<img src="'. get_template_directory_uri() .'/img/default-profile.png" class="attachment-profile-image center-block" height="100" width="100" alt="'. $post_info['post_title'] .'" />';
                    }
                ?>
    <div class="caption">
        <h3><?php echo $post_info['post_title']; ?></h3>
        <p>Skills:
            <?php 
                    foreach ($skills as $skill => $value) { 
                        echo ' '. ucwords( str_replace('-', ' ',$value['skill']) ) .', ';
                    }
                    ?>
        </p>
        <p>Years of Experience: <?php echo $info['yrs_exp']; ?></p>
        <a href="<?php echo $post_info['guid']; ?>" class="btn btn-warning">View Profile</a> </div>
</div>
<?php endif; endfor; ?>
<?php return ob_get_clean(); } 
add_shortcode('resume_portal_candidate', 'resume_portal_homepage_candidate');

// the skill row output
function resume_portal_add_skill($key, $j) { ob_start(); ?>
<tr class="skill-row text-center">
    <td class="form-group">
        <select class="form-control" name="skills-<?php echo $j; ?>" required>
            <option <?php echo $child_category->name == $key ? 'selected' : ''; ?> value="">Please Select one</option>
            <?php 
                // Get all the Skills Category
                $args = array(
                        'type'                     => 'post',
                        'child_of'                 => 0,
                        'parent'                   => '',
                        'orderby'                  => 'name',
                        'order'                    => 'ASC',
                        'hide_empty'               => 0,
                        'hierarchical'             => 1,
                        'exclude'                  => '',
                        'include'                  => '',
                        'number'                   => '',
                        'taxonomy'                 => 'skills',
                        'pad_counts'               => false 
                    ); 
                $categories = get_categories( $args );

                // Loop
                foreach ($categories as $category) {
                    // If its a parent
                    if($category->parent == 0) {
                        // Echo the <optgroup>
                        echo '<optgroup label="'. $category->cat_name .'">';

                        // Put all the Parent ID into an array
                        $parent_id = array($category->term_id);

                        // Query all the child category of the Parent using its ID
                        $args = array(
                                'parent'                   => $parent_id[0],
                                'orderby'                  => 'name',
                                'order'                    => 'ASC',
                                'hide_empty'               => 0,
                                'taxonomy'                 => 'skills'
                            ); 
                        $child_categories = get_categories( $args );

                        // Loop
                        foreach ($child_categories as $child_category) : ?>
            <option value="<?php echo $category->cat_name .'.'. $child_category->cat_name; ?>" <?php echo $child_category->name == $key['skill'] ? 'selected' : ''; ?>> <?php echo $child_category->cat_name; ?> </option>
            <?php endforeach; 
                        // Close the <optgroup>
                        echo '</optgroup>';
                    }
                }
            ?>
        </select></td>
    <td class="form-group"><label for="score">Score</label>
        <select name="score-<?php echo $j; ?>" class="score input-sm">
            <?php for( $i = 1; $i <= 10; $i++ ) : ?>
            <option <?php echo $i == $key['score'] ? 'selected' : ''; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
        </select></td>
    <td class="form-group">
        <input name="years_of_exp-<?php echo $j; ?>" id="years_of_exp" type="number" class="text-center input-medium" min="1" max="100"
        value="<?php echo $key['yrs_exp'] ? $key['yrs_exp'] : '1' ; ?>" />
        <a class="remove-row-<?php echo $j; ?> red pull-right delete-btn" href="#"> <i class="icon-trash bigger-130"></i> </a>
    </td>
</tr>
<?php return ob_get_clean(); } 

// Default Skill row
function resume_portal_default_row() {  ob_start(); ?>
<tr class="skill-row text-center">
    <td class="form-group">
        <select class="form-control" name="skills-1" required>
            <option value="" selected>Please Select one</option>
            <?php 
                // Get all the Skills Category
                $args = array(
                    'type'                     => 'post',
                    'child_of'                 => 0,
                    'parent'                   => '',
                    'orderby'                  => 'name',
                    'order'                    => 'ASC',
                    'hide_empty'               => 0,
                    'hierarchical'             => 1,
                    'exclude'                  => '',
                    'include'                  => '',
                    'number'                   => '',
                    'taxonomy'                 => 'skills',
                    'pad_counts'               => false 
                ); 
                $categories = get_categories( $args );

                // Loop
                foreach ($categories as $category) {
                    // If its a parent
                    if($category->parent == 0) {
                        // Echo the <optgroup>
                        echo '<optgroup label="'. $category->cat_name .'">';

                        // Put all the Parent ID into an array
                        $parent_id = array($category->term_id);

                        // Query all the child category of the Parent using its ID
                        $args = array(
                            'parent'                   => $parent_id[0],
                            'orderby'                  => 'name',
                            'order'                    => 'ASC',
                            'hide_empty'               => 0,
                            'taxonomy'                 => 'skills'
                        ); 
                        $child_categories = get_categories( $args );

                        // Loop
                        foreach ($child_categories as $child_category) : ?>
            <option value="<?php echo $category->cat_name .'.'. $child_category->cat_name; ?>"> <?php echo $child_category->cat_name; ?> </option>
            <?php endforeach; 
                        // Close the <optgroup>
                        echo '</optgroup>';
                    }
                }
            ?>
        </select></td>
    <td class="form-group"><label for="score">Score</label>
        <select name="score-1" class="score input-sm">
            <?php for( $i = 1; $i <= 10; $i++ ) : ?>
            <option><?php echo $i; ?></option>
            <?php endfor; ?>
        </select></td>
    <td class="form-group">
        <input name="years_of_exp-1" id="years_of_exp" type="number" class="text-center input-medium" min="1" max="100" value="1" />
        <!-- <a class="remove-row-1 red pull-right delete-btn" href="#"> <i class="icon-trash bigger-130"></i> </a> -->
    </td>
</tr>
<?php return ob_get_clean(); } 


// Add something before the form of Contact Form 7
function resume_portal_add_content( $form ) {
	global $post;
	
	$revised_form = '<input type="hidden" name="position" value="'. $post->post_title .'" />';
    $revised_form .= '<div class="resume_portal">';
    $revised_form .= '<input type="hidden" name="skill_ctr" />';
    $revised_form .= '<table id="sample-table-1" class="table table-striped table-bordered table-hover">';
    $revised_form .= '<thead><tr>';	
    $revised_form .= '<th class="text-center">Skills</th>';
    $revised_form .= '<th class="text-center">Self Assessment</th>';
    $revised_form .= '<th class="text-center">Years of Experience</th>';
    $revised_form .= '</tr></thead>';
    $revised_form .= '<tbody>'. resume_portal_default_row() .'</tbody>';
    $revised_form .= '</table>';
    $revised_form .= '<button type="button" class="btn btn-primary pull-right wpcf7-add-skill-btn">';
    $revised_form .= '<i class="glyphicon glyphicon-plus"></i> Add Skill';
    $revised_form .= '</button>';
    $revised_form .= '<hr /></div>'. $form;

    return $revised_form;
} 
add_filter( 'wpcf7_form_elements', 'resume_portal_add_content' );

// Save the Applicant to Resume Portal Custom Post type when appying for a job
function resume_portal_save_candidate() {
	//function for applicants 
	careers_application_submit( );
}
add_action('wpcf7_before_send_mail', 'resume_portal_save_candidate');


function careers_application_submit() {
	global $wpdb;
	
	// get all the users
	$user_count = resume_portal_get_all_user();

	//Table Name
	$tablename = $wpdb->prefix .'resumeportal';
	
	// Custom Post Type details
	$new_post = array(
	  'post_title'    => $_POST['full-name'],
	  'post_type'	  => 'resume_portal',
	  'post_status'   => 'publish'
	);
	
	// Insert the post into the database
	$new_id = wp_insert_post( $new_post );
	
	// putting all skills in an array
    $skill = array();
    $wp_skill = array();

    for($l = 1; $l <= 50; $l++) {
        if(isset($_POST['skills-'. $l])) {
            //option value (e.g: Group 1.option1)
            // get only the group name (e.g: Group 1)
            $group = current( explode('.', $_POST['skills-'. $l]) );
            // get only the option name (e.g: option1)
            $c_skill = substr(strrchr($_POST['skills-'. $l], '.'), 1);
            
            // push the skills in the $skill
            array_push($skill, array(
                    'group'     => $group, 
                    'skill'     => $c_skill,
                    'score'     => $_POST['score-'. $l],
                    'yrs_exp'   => $_POST['years_of_exp-'. $l] 
                )
            );

            // push the skills in the $wp_array
            array_push( $wp_skill, $c_skill );  
        }
    }

    // Saving the Custom Taxonomy for the wordpress DB
    wp_set_object_terms($new_id, $wp_skill, 'skills');
	
	// Form Details
	$data = array(
		'id'            	=> $new_id,
		'c_name'        	=> $_POST['full-name'],
		'published'     	=> 'N',
		'c_number'      	=> $user_count + 1,
		'c_position'    	=> $_POST['position'],
		'yrs_exp'       	=> $_POST['yrs-exp'],
		'education'     	=> $_POST['university'],
		'degree'        	=> $_POST['degree'],
		'yrs_grad'      	=> $_POST['graduated'],
		'email'         	=> $_POST['email'],
		'phone'         	=> $_POST['phone'],
		'availability'  	=> 'ASAP',
		'location'			=> $_POST['location'],
		'salary'        	=> $_POST['salary'],
		'expected_salary' 	=> $_POST['expected-salary'],
		'notice' 			=> $_POST['notice'],
		'code_sample'   	=> $_POST['code'],
		'skills'        	=> json_encode( $skill )
	);
	$format = array( '%d', '%s', '%s', '%d', '%s', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%s', '%s', '%s' );
	
	// upload resume function
    // Make sure the file array isn't empty
    if(!empty($_FILES['resume_upload']['name'])) {
        // Setup the array of supported file types.
        $supported_types = array(
            'application/pdf', //pdf
            'application/msword', // doc
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' // docx
        );

        // Get the file type of the upload
        $arr_file_type = wp_check_filetype(basename($_FILES['resume_upload']['name']));
        $uploaded_type = $arr_file_type['type'];

        // Check if the type is supported. If not, throw an error.
        if(in_array($uploaded_type, $supported_types)) {

            // Use the WordPress API to upload the file
            $upload = wp_upload_bits( $_FILES['resume_upload']['name'], null, file_get_contents($_FILES['resume_upload']['tmp_name']) );
            
            array_push($upload, $upload['name'] = $_FILES['resume_upload']['name']);

            if(isset($upload['error']) && $upload['error'] != 0) {
                wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
            }
            else {
                update_post_meta($new_id, 'resume_upload', $upload);
            }
        } else {
            wp_die("The file type that you've uploaded is not supported.");
        }
    }

	// Insert Query
	$wpdb->replace($tablename, $data, $format);
}

// like var_dump()
function debug($arr) {
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}